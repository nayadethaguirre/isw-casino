
    drop table public.Producto

    create table public.Producto (
        idProducto int4 not null unique,
        categoriaProducto varchar(20) not null,
        descripcion varchar(60) not null,
        nombreProducto varchar(30) not null,
        rutaImagen varchar(60),
        stock int4 not null,
        valor int4 not null,
        primary key (idProducto)
    )
