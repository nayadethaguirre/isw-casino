package gui;

import javax.swing.JPanel;

import cl.ufro.bd.Producto;
import cl.ufro.controller.ProductoController;

import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ItemProducto extends JPanel {
	private JPanel contentPane;
	private JLabel id;
	private JLabel nombre;
	private JLabel descripcion;
	private JLabel valor;
	private JLabel stock;
	public ItemProducto() {
		setLayout(null);
		Producto producto=new Producto();


		setLayout(null);

		JLabel lblId = new JLabel("id");
		lblId.setBounds(22, 30, 70, 15);
		add(lblId);

		//Dynamic
		id = new JLabel("");
		id.setText(" ");
		id.setBounds(160, 30, 115, 15);
		add(id);

		JLabel lblNombre = new JLabel("nombre");
		lblNombre.setBounds(22, 55, 70, 15);
		add(lblNombre);

		//Dynamic
		nombre = new JLabel("");
		nombre.setText(producto.getNombreProducto());
		nombre.setBounds(160, 55, 115, 15);
		add(nombre);

		JLabel lblDescripcion = new JLabel("Descripción");
		lblDescripcion.setBounds(22, 82, 115, 15);
		add(lblDescripcion);

		//Dynamic
		descripcion = new JLabel("");
		descripcion.setText(producto.getDescripcion());
		descripcion.setBounds(160, 82, 115, 15);
		add(descripcion);


		JLabel lblValor = new JLabel("Valor");
		lblValor.setBounds(22, 115, 70, 15);
		add(lblValor);

		//Dynamic
		valor = new JLabel("");
		valor.setText(" ");
		valor.setBounds(160, 115, 115, 15);
		add(valor);


		JLabel lblStock = new JLabel("Stock");
		lblStock.setBounds(22, 142, 70, 15);
		add(lblStock);

		//Dynamic
		stock = new JLabel("");
		stock.setText(" ");
		stock.setBounds(160, 142, 115, 15);
		add(stock);
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//agregar una funcion para agregar al carrito (Seleccionar)
			}
		});
		
		
		btnAgregar.setBounds(309, 25, 117, 25);
		add(btnAgregar);


	}
}
