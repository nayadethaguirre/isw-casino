package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cl.ufro.bd.Producto;
import cl.ufro.bd.Usuario;
import cl.ufro.controller.UsuarioController;
import cl.ufro.utils.ManagerSession;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class PanelWeb extends JPanel{

	private Principal principal;
	private PanelMenu panelMenu;
	private PanelHeader panelHeader;
	private PanelBody panelBody;
	private PanelHome panelHome;
	private ItemProducto itemProducto;
	public PanelWeb(Principal principal) {
		this();
		this.principal=principal;
	}

	public PanelWeb() {
		setLayout(null);
		setBounds(0, 0, Principal.X, Principal.Y);

		panelMenu = new PanelMenu(this);
		panelMenu.setBounds(5, 90, 150, 550);
		add(panelMenu);

		panelHeader = new PanelHeader();
		panelHeader.setBounds(5, 5, 1014, 80);
		add(panelHeader);

		// Vistas de usuario
		panelBody = new PanelBody(this);
		panelBody.setBounds(160, 90, 850, 550);
		add(panelBody);

		panelHome = new PanelHome();
		panelHome.setBounds(160, 90, 850, 550);
		add(panelHome);

	}	

	public void init() {
		verPanelHome();
	}

	public void verPanelBody() {
		panelHome.setVisible(false);
		panelBody.init();
		panelBody.setVisible(true);
	}

	public void verPanelHome() {
		panelBody.setVisible(false);
		panelHome.init();
		panelHome.setVisible(true);
	}

	public void VerPanelLogin() {
		principal.verPanelLogin();
	}

}


