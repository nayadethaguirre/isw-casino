package gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

//import cl.ufro.bd.Producto;

public class Principal extends JFrame{

	final static  int X=1024;
	final static  int Y=650;

	JPanel contentPane;
	private PanelLogin panelLogin;
	private PanelWeb panelWeb;
	//private ListaProductoPanel listaProductoPanel;

	public static void main(String[] args) {
		Principal frame = new Principal();
		frame.setVisible(true);
	}

	public Principal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 250, X, Y);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		panelLogin = new PanelLogin(this);
		panelLogin.setBounds(0, 0, X, Y);
		panelLogin.setVisible(false);
		contentPane.add(panelLogin);
		
		panelWeb = new PanelWeb(this);
		panelWeb.setBounds(0, 0, X, Y);
		panelWeb.setVisible(false);
		contentPane.add(panelWeb);
		
		verPanelLogin();
	}
	public void verPanelLogin() {
		panelWeb.setVisible(false);
		
		panelLogin.setVisible(true);
		panelLogin.init();
	}

	public void verPanelWeb() {
		panelLogin.setVisible(false);
		panelWeb.setVisible(true);
		panelWeb.init();
	}
}


