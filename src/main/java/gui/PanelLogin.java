package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cl.ufro.bd.Usuario;
import cl.ufro.controller.UsuarioController;
import cl.ufro.utils.ManagerSession;

public class PanelLogin extends JPanel{
	private JTextField txtRut;
	private JTextField txtClave;

	private Principal principal;

	public PanelLogin(Principal principal) {
		this();
		this.principal=principal;

	}
	public PanelLogin() {
		setLayout(null);
		setBounds(0, 0, Principal.X, Principal.Y);

		JLabel lblRut = new JLabel("Rut:");
		lblRut.setBounds(420, 181, 66, 15);
		add(lblRut);

		JLabel lblClave = new JLabel("Clave:");
		lblClave.setBounds(420, 237, 66, 15);
		add(lblClave);

		txtRut = new JTextField();
		txtRut.setBounds(504, 179, 124, 19);
		add(txtRut);
		txtRut.setColumns(10);

		txtClave = new JTextField();
		txtClave.setBounds(504, 235, 124, 19);
		add(txtClave);
		txtClave.setColumns(10);

		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

//				se crea el usuario vacio y se llena con los datos que aparecen en la gui
				Usuario usuario = new Usuario();
				usuario.setRut(txtRut.getText());
				usuario.setClave(txtClave.getText());
				
				UsuarioController controller = new UsuarioController();
				usuario = controller.identificar(usuario);				
								
				if(usuario == null) {
					principal.verPanelLogin();
					System.out.println("Ingreso inválido!");
				}else {
					ManagerSession.saveObject("usuario", usuario);
					principal.verPanelWeb();
				}
			}
		});
		btnEntrar.setBounds(430, 307, 114, 25);
		add(btnEntrar);
	}

	

	public void init() {
		txtRut.setText("");
		txtClave.setText("");
	}
	
}


