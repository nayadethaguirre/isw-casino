package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import cl.ufro.bd.Usuario;
import cl.ufro.controller.UsuarioController;
import cl.ufro.utils.ManagerSession;

public class PanelHome extends JPanel{

	private Principal principal;

	public PanelHome(Principal principal) {
		this();
		this.principal=principal;

	}
	public PanelHome() {
		setLayout(null);
		setBounds(0, 0, 850,550);
		setBorder(new LineBorder(new Color(0, 0, 0)));
		
		JLabel lblHome = new JLabel("home");
		lblHome.setBounds(26, 28, 70, 15);
		add(lblHome);

	}	

	public void init() {

	}
}


