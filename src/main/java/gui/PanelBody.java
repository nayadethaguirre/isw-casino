package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import cl.ufro.bd.Producto;
import cl.ufro.bd.Usuario;
import cl.ufro.controller.ProductoController;
import cl.ufro.controller.UsuarioController;
import cl.ufro.utils.ManagerSession;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JTable;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PanelBody extends JPanel{

	final static  int X=1024;
	final static  int Y=650;
	private PanelWeb panelWeb;
	//private JTable table;
	private JPanel contentPane;

	public PanelBody(PanelWeb panelWeb) {
		this();

		this.panelWeb = panelWeb;
	}
	public PanelBody() {

		setLayout(null);
		setBounds(0, 0, 850,550);
		setBorder(new LineBorder(new Color(0, 0, 0)));

		JLabel lblListaProductos = new JLabel("Lista de Productos");
		lblListaProductos.setBounds(88, 101, 211, 20);
		add(lblListaProductos);

		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelWeb.VerPanelLogin();
			}
		});
		btnSalir.setBounds(445, 23, 117, 25);
		add(btnSalir);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(88, 133, 616, 266);
		add(scrollPane);
		contentPane = new JPanel();
		scrollPane.setViewportView(contentPane);
		contentPane.setLayout(null);

	}	


	public void init() {
		ProductoController productoController =new ProductoController();
		List<Producto> productos = productoController.listarProductos();
		for (int i = 0; i < productos.size(); i++) {
			
			ProductoPanel productoPanel=new ProductoPanel(productos.get(i));
			productoPanel.setLocation(0,ProductoPanel.HEIGHT*i);
			contentPane.add(productoPanel);
		}
		contentPane.setPreferredSize(new Dimension(ProductoPanel.WIDTH, ProductoPanel.HEIGHT*productos.size()));
		contentPane.repaint();
	}
}


