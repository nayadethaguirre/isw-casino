package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import cl.ufro.bd.Usuario;
import cl.ufro.controller.UsuarioController;
import cl.ufro.utils.ManagerSession;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PanelMenu extends JPanel{

	private PanelWeb panelWeb;

	public PanelMenu(PanelWeb panelWeb) {
		this();
		this.panelWeb=panelWeb;

	}
	public PanelMenu() {
		setLayout(null);
		setBounds(0, 0, 150, 550);
		setBorder(new LineBorder(new Color(0, 0, 0)));
		
		JLabel lblMenu = new JLabel("Menu");
		lblMenu.setBounds(53, 12, 70, 15);
		add(lblMenu);
		
		JLabel lblHome = new JLabel("Home");
		lblHome.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("Home");
				panelWeb.verPanelHome();
			}
		});
		lblHome.setBounds(12, 39, 70, 15);
		add(lblHome);
		
		JLabel lblBody = new JLabel("Listar Productos");
		lblBody.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("Productos");
				panelWeb.verPanelBody();
			}
		});
		lblBody.setBounds(12, 65, 126, 15);
		add(lblBody);

	}	

	public void init() {

	}
	
}


