package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import cl.ufro.bd.Usuario;
import cl.ufro.controller.UsuarioController;
import cl.ufro.utils.ManagerSession;
import java.awt.Font;

public class PanelHeader extends JPanel{

	private Principal principal;

	public PanelHeader(Principal principal) {
		this();
		this.principal=principal;

	}
	public PanelHeader() {
		setLayout(null);
		setBounds(0, 0, 1014, 80);
		setBorder(new LineBorder(new Color(0, 0, 0)));
		
		JLabel lblTitulo = new JLabel("Casino Universidad de La Frontera");
		lblTitulo.setFont(new Font("Dialog", Font.BOLD, 18));
		lblTitulo.setBounds(28, 32, 386, 22);
		add(lblTitulo);

	}	

	public void init() {

	}
}


