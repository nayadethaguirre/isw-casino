package gui;

import javax.swing.JPanel;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import cl.ufro.bd.Producto;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ProductoPanel extends JPanel {
	private PanelWeb panelWeb;

	private Producto producto;
	
	private JLabel id;
	private JLabel nombre;
	private JLabel descripcion;
	private JLabel categoria;
	private JLabel stock;
	private JLabel valor;
	//private Jlabel rutaImagen;
	
	public static int HEIGHT=275;
	public static int WIDTH = 595;
	
	/**
	 * @wbp.parser.constructor
	 */
	public ProductoPanel(PanelWeb panelWeb, Producto producto) {
		this(producto);
		
		this.panelWeb = panelWeb;
	}
	
	public ProductoPanel(Producto producto) {
		setLayout(null);
		setBounds(0, 0, 595, 314);
		setBorder(new LineBorder(new Color(0, 0, 0)));
		
		JLabel lblIdproducto = new JLabel("idProducto");
		lblIdproducto.setBounds(33, 30, 200,50);
		add(lblIdproducto);
		
		JLabel lblNombre = new JLabel(" Nombre");
		lblNombre.setBounds(33, 70, 200, 50);
		add(lblNombre);
		
		JLabel lblCategoria = new JLabel(" Categoria");
		lblCategoria.setBounds(33, 110, 200, 50);
		add(lblCategoria);
		
		JLabel lblDescripcion = new JLabel(" Descripcion");
		lblDescripcion.setBounds(33, 150, 200, 50);
		add(lblDescripcion);

		JLabel lblStock = new JLabel(" Stock");
		lblStock.setBounds(33, 190, 200, 50);
		add(lblStock);
		
		JLabel lblValor = new JLabel(" Valor");
		lblValor.setBounds(33, 230, 200, 50);
		add(lblValor);
		
		id = new JLabel();
		id.setBounds(200, 30, 200, 50);
		add(id);
		
		nombre=new JLabel();
		nombre.setBounds(200,70,200,50);
		add(nombre);
		
		categoria=  new JLabel();
		categoria.setBounds(200,110,200,50);
		add(categoria);
		
		descripcion=new JLabel();
		descripcion.setBounds(200,150,200,50);
		add(descripcion);
		
		stock=new JLabel();
		stock.setBounds(200,190,200,50);
		add(stock);
		
		valor=new JLabel();
		valor.setBounds(200,230,200,50);
		add(valor);
		
		JButton btnVolver = new JButton("Agregar");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelWeb.verPanelBody();
			}
		});
		btnVolver.setBounds(418, 236, 100, 39);
		add(btnVolver);
		
		id.setText(String.valueOf(producto.getIdProducto()));
		nombre.setText(producto.getNombreProducto());
		descripcion.setText(producto.getDescripcion());
		categoria.setText(producto.getCategoriaProducto());
		stock.setText(String.valueOf(producto.getStock()));
		valor.setText(String.valueOf(producto.getValor()));
	}
	/*
	public void setProducto(Producto producto) {
		this.producto=producto;
		assignFields(producto);

		System.out.println(producto);
	}
	
	private void assignFields(Producto producto) {
		id.setText(String.valueOf(producto.getIdProducto()));
		nombre.setText(producto.getNombreProducto());
		descripcion.setText(producto.getDescripcion());
		categoria.setText(producto.getCategoriaProducto());
		stock.setText(String.valueOf(producto.getStock()));
		valor.setText(String.valueOf(producto.getValor()));
		
	}*/
}
