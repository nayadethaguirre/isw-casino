package cl.ufro.bd;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
@Entity(name = "producto")
@Table (name ="Producto")
public class Producto {
	public Producto () { };

	@Id
	@Column(name ="idProducto", unique=true,nullable=false) // ,nullable=false
	private int idProducto;
	@Column(name="categoriaProducto", length =20,nullable=false)
	private String categoriaProducto;
	@Column(name ="nombreProducto",length=30,nullable=false)
	private String nombreProducto;
	@Column(name ="stock",nullable=false)
	private int stock;  
	@Column(name ="descripcion",length=60,nullable=false)
	private String descripcion;
	@Column(name ="valor",nullable=false)
	private int valor;
	@Column(name ="rutaImagen", length=60)
	private String rutaImagen;
	
	/*
	@OneToMany(targetEntity=Producto.class, mappedBy="idProducto", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@OnDelete(action = OnDeleteAction.CASCADE)
	List <Producto> listaProductos = new ArrayList<Producto>();
	*/
	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}
	public String getCategoriaProducto() {
		return categoriaProducto;
	}

	public void setCategoriaProducto(String categoriaProducto) {
		this.categoriaProducto = categoriaProducto;
	}
	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public String getRutaImagen() {
		return rutaImagen;
	}

	public void setRutaImagen(String rutaImagen) {
		this.rutaImagen = rutaImagen;
	}


}
