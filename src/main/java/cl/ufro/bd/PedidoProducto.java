package cl.ufro.bd;



import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import antlr.collections.List;
import cl.ufro.utils.StandardClassPK;

@Entity
@Table (name ="PedidoProducto")
public class PedidoProducto {

	//HACER CLAVE COMPUESTA
	@Embeddable
	static class PedidoProductoPK extends StandardClassPK{

		@JoinColumn(referencedColumnName="Pedido.idPedido" )
		@Column(name="idPedido",nullable=false)
		private int idPedido;

		@JoinColumn(referencedColumnName="Producto.idProducto" )
		@Column(name="idProducto",nullable=false)
		private int idProducto;

	}
	@EmbeddedId
	private PedidoProductoPK pk= new PedidoProductoPK ();

	public PedidoProducto() { };
	public int getIdPedido() {
		return pk.idPedido;
	}
	public void setIdPedido(int idPedido) {
		this.pk.idPedido = idPedido;
	}
	public int getIdProducto() {
		return pk.idProducto;
	}
	public void setIdProducto(int idProducto) {
		this.pk.idProducto = idProducto;
	}

}
