package cl.ufro.controller;
import cl.ufro.bd.Pedido;
import cl.ufro.bd.Producto;
import cl.ufro.bd.Usuario;
import cl.ufro.service.PedidoService;

public class PedidoController {

  private cl.ufro.service.PedidoService pedidoService;  public PedidoController () { };
  
  //
  // Methods
  //


  /**
   * Set the value of pedidoService
   * @param newVar the new value of pedidoService
   */
  private void setPedidoService (cl.ufro.service.PedidoService newVar) {
    pedidoService = newVar;
  }

  /**
   * Get the value of pedidoService
   * @return the value of pedidoService
   */
  private cl.ufro.service.PedidoService getPedidoService () {
    return pedidoService;
  }

  //
  // Other methods
  //

  /**
   * @return       cl.ufro.bd.Pedido
   * @param        producto
   * @param        usuario
   */
  public Producto PedirProducto(cl.ufro.bd.Producto producto, cl.ufro.bd.Usuario usuario)
  {
	return producto;
  }


}
