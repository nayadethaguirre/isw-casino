package cl.ufro.controller;

import java.util.List;

import cl.ufro.bd.Producto;
import cl.ufro.service.ProductoService;

public class ProductoController {
	private ProductoService productoService = new ProductoService();
	
	public ProductoController () { };

	public Producto identificar(Producto producto){
		return productoService.identificar(producto);
	}

	public List<Producto> listarProductos() {
		return productoService.listarProductos();		
	}

	public Producto identificar(int idProducto){
		return productoService.identificarPorId(idProducto);
	}
}
