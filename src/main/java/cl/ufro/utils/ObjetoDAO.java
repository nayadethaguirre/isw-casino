package cl.ufro.utils;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;

public class ObjetoDAO {
	private Session session = null; 
	private Transaction tx = null;
	private static AnnotationConfiguration config = null;
	private static SessionFactory sessionFactory = null;

	public void add(Object registro){
		try{ 
			startOperationBD();
			sessionFactory.getCurrentSession().beginTransaction();
			sessionFactory.getCurrentSession().save(registro);
			sessionFactory.getCurrentSession().getTransaction().commit();
		} catch (Exception he){
			sessionFactory.getCurrentSession().getTransaction().rollback();
			he.printStackTrace();
		} 
	} 
	public void update(Object registro){
		try{ 
			startOperationBD(); 
			sessionFactory.getCurrentSession().beginTransaction();
			sessionFactory.getCurrentSession().update(registro); 
			sessionFactory.getCurrentSession().getTransaction().commit();
		} catch (Exception he){
			sessionFactory.getCurrentSession().getTransaction().rollback();
			he.printStackTrace();
		} 
	}

	public void remove(Object registro){ 
		try{ 
			startOperationBD(); 
			sessionFactory.getCurrentSession().beginTransaction();
			sessionFactory.getCurrentSession().delete(registro); 
			sessionFactory.getCurrentSession().getTransaction().commit();
		} catch (Exception he){
			sessionFactory.getCurrentSession().getTransaction().rollback();
			he.printStackTrace();
		} 
	}
	public List<?> selectQuery(String sqlQuery){ 
		List<?> list=null;
		try{ 
			startOperationBD();
			sessionFactory.getCurrentSession().beginTransaction();
			list=sessionFactory.getCurrentSession().createQuery(sqlQuery).list();
			sessionFactory.getCurrentSession().getTransaction().commit();
		} catch (Exception he){
			sessionFactory.getCurrentSession().getTransaction().rollback();
			list=new ArrayList<Object>();
			he.printStackTrace();
		} 
		return list; 
	}
	public List<?> selectQuery(String sqlQuery, int maxResult){ 
		List<?> list=null;
		try{ 
			startOperationBD();
			sessionFactory.getCurrentSession().beginTransaction();
			Query query = sessionFactory.getCurrentSession().createQuery(sqlQuery);
			query.setMaxResults(maxResult);
			list=query.list();
			sessionFactory.getCurrentSession().getTransaction().commit();
		} catch (Exception he){
			sessionFactory.getCurrentSession().getTransaction().rollback();
			list=new ArrayList<Object>();
			he.printStackTrace();
		} 
		return list; 
	}
	public Object selectSQLQuery(String sqlQuery){ 
		Object object = null;
		try{ 
			startOperationBD();
			sessionFactory.getCurrentSession().beginTransaction();
			SQLQuery query =sessionFactory.getCurrentSession().createSQLQuery(sqlQuery);
			object= query.uniqueResult();
			sessionFactory.getCurrentSession().getTransaction().commit();
		} catch (Exception he){
			sessionFactory.getCurrentSession().getTransaction().rollback();
			he.printStackTrace();
		} 
		return object;
	}

	public void executeSQLQuery(String sqlQuery){ 
		try{ 
			startOperationBD();
			sessionFactory.getCurrentSession().beginTransaction();
			SQLQuery query =sessionFactory.getCurrentSession().createSQLQuery(sqlQuery);
			query.executeUpdate();
			sessionFactory.getCurrentSession().getTransaction().commit();
		} catch (Exception he){
			sessionFactory.getCurrentSession().getTransaction().rollback();
			he.printStackTrace();
		} 
	}

	protected Session getSession(){
		if(session.isConnected() || session.isOpen())
			session.close();
		session =  sessionFactory.openSession();
		tx = session.beginTransaction();
		return session;
	}
	protected Transaction getTransaction(){
		return sessionFactory.getCurrentSession().getTransaction();
	}
	protected void startOperationBD() throws HibernateException{ 
		if(config==null){
			config = new AnnotationConfiguration();
			config.configure("hibernate.cfg.xml");
			sessionFactory = config.buildSessionFactory();
			session =  sessionFactory.openSession();
			tx = session.beginTransaction();
			tx.setTimeout(0);
		}		
	}   
}