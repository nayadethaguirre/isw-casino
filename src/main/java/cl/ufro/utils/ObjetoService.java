package cl.ufro.utils;

public abstract class ObjetoService {
	
	private ObjetoDAO objetoDAO = new ObjetoDAO();
	
	public void add(Object object) {
		objetoDAO.add(object);
	}
	public void update(Object object) {
		objetoDAO.update(object);		
	}
	public void remove(Object object) {
		objetoDAO.remove(object);		
	}
}

