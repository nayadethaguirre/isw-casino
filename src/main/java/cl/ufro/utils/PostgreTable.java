package cl.ufro.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

public abstract class PostgreTable {
	
	public static void main(String[] args){
		//se ejecutan comandos SQL antes de reconstruir la base de datos
		PostgreTable.executeSQLfile("./src/main/resources/before.sql");
		//se borra y se crea nuevamente la base de datos.
		//el esquema se guarda en "sql/export.sql"
		//se importan los registros existentes en "src/main/resources/import.sql"
		PostgreTable.makeBD("hibernate.cfg.xml","./sql/export.sql");
	}
	public static void makeBD(String configHibernate, String dataBase){
		AnnotationConfiguration config = new AnnotationConfiguration();
		config.configure(configHibernate);
		SchemaExport schemaExport = new SchemaExport(config);
		schemaExport.setOutputFile(dataBase);
		schemaExport.create(true, true);//imprime y exporta
	}
	private static void executeSQLfile(String file){
		ObjetoDAO objetoDAO = new ObjetoDAO();
		try{
			File archivo = new File(file);
			FileReader fr = new FileReader (archivo);
			BufferedReader br = new BufferedReader(fr);
			String linea;
			while((linea=br.readLine())!=null){
				if(!(linea.equals(""))){
					if(!(linea.substring(0, 2).equals("--")) || (linea.substring(0, 3).equals("--#")))
						System.out.println(linea);
					if(!(linea.substring(0, 2).equals("--")))
						objetoDAO.executeSQLQuery(linea);
				}
			}
		}catch (Exception e) {e.printStackTrace();}
	}
}
