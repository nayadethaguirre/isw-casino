package cl.ufro.utils;

import java.io.Serializable;

public abstract class StandardClassPK implements Serializable{
	private static final long serialVersionUID = 1L;
	//Evita Warnings en Hibernate. - Desabilita la comparaci�n de objetos con "equals"
	@Override
	public boolean equals(final Object obj){ return false;}
	@Override
	public int hashCode(){ return 0;}
}
