package cl.ufro.dao;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;

import cl.ufro.bd.Producto;
import cl.ufro.utils.ObjetoDAO;

public class ProductoDao extends ObjetoDAO{

  public ProductoDao () { };
  
  public Producto find(Producto producto) {
	  
			String query="FROM "+producto.getClass().getName()+" WHERE idProducto="+producto.getIdProducto();
	//nombre a la entidad entonces se evita la consulta via referencia de clases
	  //String query = "select product from producto product where product.idProducto = " + producto.getIdProducto() ;
	  System.out.println(query);
			List<Producto> lista = (List<Producto>)selectQuery(query);
			if(lista.size()>0)
				return lista.get(0);
			return null;		
		}
  /*falta listar todos*/
  public List<Producto> listar(){
	  String query="FROM "+Producto.class.getName()+" order by nombreProducto desc";
	  //String query= "select product from producto product order by product.nombreProducto";
	  System.out.println(query);
			  List<?> productos = selectQuery(query);
			return (List<Producto>)(productos);
	
  
	}
  
  }



