package cl.ufro.service;

import java.util.List;

import cl.ufro.bd.Producto;
import cl.ufro.dao.ProductoDao;

public class ProductoService {

	private ProductoDao productoDao = new ProductoDao();

	public Producto identificar(Producto producto){
		Producto productoBd = productoDao.find(producto);
		if(productoBd != null && productoBd.getIdProducto()==producto.getIdProducto())
			return productoBd;
		return null;
	}
	
	public List<Producto> listarProductos() {
		return productoDao.listar();
	}
	
	public Producto identificarPorId(int idProducto) {
		Producto producto = new Producto();
		producto.setIdProducto(idProducto);
		
		return identificar(producto);
	}
	
	
	
	
}