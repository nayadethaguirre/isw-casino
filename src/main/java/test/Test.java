package test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.List;


import cl.ufro.bd.Producto;
import cl.ufro.bd.Usuario;
import cl.ufro.dao.ProductoDao;
import cl.ufro.dao.UsuarioDao;

public class Test {

	public static void main(String[] args) {


		ProductoDao productoDao=new ProductoDao();
		Producto producto =new Producto();
		Producto producto1 =new Producto();
		Producto producto2 =new Producto();


		producto.setIdProducto(123456789);
		producto.setCategoriaProducto("Bebestibles");
		producto.setNombreProducto("Bebida");
		producto.setDescripcion("Sprite 225ml");
		producto.setValor(1000);
		producto.setStock(10);

		producto1.setIdProducto(01234567);
		producto1.setCategoriaProducto("Snacks");
		producto1.setNombreProducto("Galletas");
		producto1.setDescripcion("Oreo 500 gr");
		producto1.setValor(850);
		producto1.setStock(20);

		producto2.setIdProducto(75456789);
		producto2.setCategoriaProducto("Bebestible");
		producto2.setNombreProducto("Cafe");
		producto2.setDescripcion("Latte");
		producto2.setValor(1350);
		producto2.setStock(150);

		
		productoDao.add(producto);
		productoDao.add(producto1);
		productoDao.add(producto2);

		
		
		
		System.out.println("Productos Ingresado!");
		
		
		List<Producto> productos= productoDao.listar();
		System.out.println(productos);
		
		for (int i = 0; i < productos.size(); i++) {
			System.out.println();
		}
		
	}

}
