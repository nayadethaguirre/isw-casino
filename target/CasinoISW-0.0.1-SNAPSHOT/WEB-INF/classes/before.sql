--# BORRA EL SCHEMA Y LO CREA NUEVAMENTE
--# Evita errores por la modificaci�n de claves foraneas
--# Para que funcione el dueño del schema debe ser "isw"
--# Comando SQL:		ALTER SCHEMA public OWNER TO isw;
DROP SCHEMA casino CASCADE;
CREATE SCHEMA public AUTHORIZATION isw;
GRANT ALL ON SCHEMA public TO isw;
COMMENT ON SCHEMA public IS 'standard public schema';
