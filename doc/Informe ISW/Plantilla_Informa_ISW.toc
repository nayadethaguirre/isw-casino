\babel@toc {spanish}{}
\contentsline {chapter}{\'Indice de Figuras}{III}{section*.1}
\contentsline {chapter}{\'Indice de Tablas}{IV}{section*.2}
\contentsline {chapter}{\numberline {Cap\'itulo\hspace {0.2cm} I}Introducci\'on}{1}{chapter.1}
\contentsline {chapter}{\numberline {Cap\'itulo\hspace {0.2cm} II}An\'alisis}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}Contexto}{2}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Presentaci\'on general de la situaci\'on actual}{2}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Descripci\'on detallada del software}{5}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Descripci\'on de clientes y usuarios}{6}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}Especificaci\'on de requerimientos}{6}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Objetivos Generales}{6}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Lista de funciones del sistema}{7}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Lista de atributos del sistema}{7}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Lista de atributos del sistema por funcionalidad}{8}{subsection.2.2.4}
\contentsline {section}{\numberline {2.3}Actores}{9}{section.2.3}
\contentsline {chapter}{\numberline {Cap\'itulo\hspace {0.2cm} III}Casos de Uso}{11}{chapter.3}
\contentsline {section}{\numberline {3.1}Presentaci'general de los Casos de Uso}{11}{section.3.1}
\contentsline {section}{\numberline {3.2}Diagrama de Casos de Uso}{13}{section.3.2}
\contentsline {section}{\numberline {3.3}Descripci\'on textual y gr\'afica de los Casos de Uso}{14}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Descripci'del Caso de Uso: \textit {Nombre de Caso}}{14}{subsection.3.3.1}
