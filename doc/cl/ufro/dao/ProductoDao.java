package cl.ufro.dao;

import java.util.*;
import cl.ufro.bd.Producto;

public class ProductoDao {

  //
  // Fields
  //

  private cl.ufro.bd.Producto producto;  public ProductoDao () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of producto
   * @param newVar the new value of producto
   */
  private void setProducto (cl.ufro.bd.Producto newVar) {
    producto = newVar;
  }

  /**
   * Get the value of producto
   * @return the value of producto
   */
  private cl.ufro.bd.Producto getProducto () {
    return producto;
  }

  //
  // Other methods
  //

  /**
   * @return       cl.ufro.bd.Producto
   * @param        producto
   */
  public cl.ufro.bd.Producto find(cl.ufro.bd.Producto producto)
  {
  }


  /**
   */
  public void ProductoDao()
  {
  }


  /**
   * @param        producto
   */
  public void listarProductos(cl.ufro.bd.Producto producto)
  {
  }


}
