package cl.ufro.controller;
import cl.ufro.bd.Vendedor;
import cl.ufro.bd.Pedido;
import cl.ufro.service.VendedorService;

public class VendedorController {

  private cl.ufro.service.VendedorService vendedorService;  public VendedorController () { };
  
  //
  // Methods
  //


  /**
   * Set the value of vendedorService
   * @param newVar the new value of vendedorService
   */
  private void setVendedorService (cl.ufro.service.VendedorService newVar) {
    vendedorService = newVar;
  }

  /**
   * Get the value of vendedorService
   * @return the value of vendedorService
   */
  private cl.ufro.service.VendedorService getVendedorService () {
    return vendedorService;
  }

  //
  // Other methods
  //

  /**
   * @return       cl.ufro.bd.Vendedor
   * @param        vendedor
   */
  public cl.ufro.bd.Vendedor identificar(cl.ufro.bd.Vendedor vendedor)
  {
  }


  /**
   * @return       boolean
   * @param        idVoucher
   */
  public boolean atenderComanda(cl.ufro.bd.Pedido idVoucher)
  {
  }


  /**
   * @return       cl.ufro.bd.Pedido
   * @param        idVoucher
   */
  public cl.ufro.bd.Pedido finalizarComanda(cl.ufro.bd.Pedido idVoucher)
  {
  }


}
