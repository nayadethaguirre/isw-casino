package cl.ufro.controller;
import cl.ufro.bd.Usuario;
import cl.ufro.service.UsuarioService;

public class UsuarioController {

  private cl.ufro.service.UsuarioService usuarioService;  public UsuarioController () { };
  
  //
  // Methods
  //


  /**
   * Set the value of usuarioService
   * @param newVar the new value of usuarioService
   */
  private void setUsuarioService (cl.ufro.service.UsuarioService newVar) {
    usuarioService = newVar;
  }

  /**
   * Get the value of usuarioService
   * @return the value of usuarioService
   */
  private cl.ufro.service.UsuarioService getUsuarioService () {
    return usuarioService;
  }

  //
  // Other methods
  //

  /**
   * @return       cl.ufro.bd.Usuario
   * @param        usuario
   */
  public cl.ufro.bd.Usuario identificar(cl.ufro.bd.Usuario usuario)
  {
  }


}
