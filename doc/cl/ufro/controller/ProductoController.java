package cl.ufro.controller;
import cl.ufro.service.ProductoService;

public class ProductoController {

  private cl.ufro.service.ProductoService productoService;  public ProductoController () { };
  
  //
  // Methods
  //


  /**
   * Set the value of productoService
   * @param newVar the new value of productoService
   */
  private void setProductoService (cl.ufro.service.ProductoService newVar) {
    productoService = newVar;
  }

  /**
   * Get the value of productoService
   * @return the value of productoService
   */
  private cl.ufro.service.ProductoService getProductoService () {
    return productoService;
  }

  //
  // Other methods
  //

  /**
   */
  public void listarProductos()
  {
  }


}
