package cl.ufro.bd;

import java.util.*;

public class Producto {

  //
  // Fields
  //

  private int idProducto;
  private String nombreProducto;
  private int stock;
  private String descripcion;
  private int valor;
  private String rutaImagen;  public Producto () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of idProducto
   * @param newVar the new value of idProducto
   */
  private void setIdProducto (int newVar) {
    idProducto = newVar;
  }

  /**
   * Get the value of idProducto
   * @return the value of idProducto
   */
  private int getIdProducto () {
    return idProducto;
  }

  /**
   * Set the value of nombreProducto
   * @param newVar the new value of nombreProducto
   */
  private void setNombreProducto (String newVar) {
    nombreProducto = newVar;
  }

  /**
   * Get the value of nombreProducto
   * @return the value of nombreProducto
   */
  private String getNombreProducto () {
    return nombreProducto;
  }

  /**
   * Set the value of stock
   * @param newVar the new value of stock
   */
  private void setStock (int newVar) {
    stock = newVar;
  }

  /**
   * Get the value of stock
   * @return the value of stock
   */
  private int getStock () {
    return stock;
  }

  /**
   * Set the value of descripcion
   * @param newVar the new value of descripcion
   */
  private void setDescripcion (String newVar) {
    descripcion = newVar;
  }

  /**
   * Get the value of descripcion
   * @return the value of descripcion
   */
  private String getDescripcion () {
    return descripcion;
  }

  /**
   * Set the value of valor
   * @param newVar the new value of valor
   */
  private void setValor (int newVar) {
    valor = newVar;
  }

  /**
   * Get the value of valor
   * @return the value of valor
   */
  private int getValor () {
    return valor;
  }

  /**
   * Set the value of rutaImagen
   * @param newVar the new value of rutaImagen
   */
  private void setRutaImagen (String newVar) {
    rutaImagen = newVar;
  }

  /**
   * Get the value of rutaImagen
   * @return the value of rutaImagen
   */
  private String getRutaImagen () {
    return rutaImagen;
  }

}
