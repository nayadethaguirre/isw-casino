package cl.ufro.bd;

public class Usuario {

  private String rut;
  private String nombre;
  private String clave;  public Usuario () { };
  /**
   * Set the value of rut
   * @param newVar the new value of rut
   */
  private void setRut (String newVar) {
    rut = newVar;
  }

  /**
   * Get the value of rut
   * @return the value of rut
   */
  private String getRut () {
    return rut;
  }

  /**
   * Set the value of nombre
   * @param newVar the new value of nombre
   */
  private void setNombre (String newVar) {
    nombre = newVar;
  }

  /**
   * Get the value of nombre
   * @return the value of nombre
   */
  private String getNombre () {
    return nombre;
  }

  /**
   * Set the value of clave
   * @param newVar the new value of clave
   */
  private void setClave (String newVar) {
    clave = newVar;
  }

  /**
   * Get the value of clave
   * @return the value of clave
   */
  private String getClave () {
    return clave;
  }

}
