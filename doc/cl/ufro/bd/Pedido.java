package cl.ufro.bd;

import java.util.*;

public class Pedido {

  //
  // Fields
  //

  private int idPedido;
  private void hora;
  private String rutUsuario;  public Pedido () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of idPedido
   * @param newVar the new value of idPedido
   */
  private void setIdPedido (int newVar) {
    idPedido = newVar;
  }

  /**
   * Get the value of idPedido
   * @return the value of idPedido
   */
  private int getIdPedido () {
    return idPedido;
  }

  /**
   * Set the value of hora
   * @param newVar the new value of hora
   */
  private void setHora (void newVar) {
    hora = newVar;
  }

  /**
   * Get the value of hora
   * @return the value of hora
   */
  private void getHora () {
    return hora;
  }

  /**
   * Set the value of rutUsuario
   * @param newVar the new value of rutUsuario
   */
  private void setRutUsuario (String newVar) {
    rutUsuario = newVar;
  }

  /**
   * Get the value of rutUsuario
   * @return the value of rutUsuario
   */
  private String getRutUsuario () {
    return rutUsuario;
  }

}
