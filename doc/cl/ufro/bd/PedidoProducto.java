package cl.ufro.bd;

import java.util.*;

public class PedidoProducto {

  //
  // Fields
  //

  private int idPedido;
  private int idProducto;  public PedidoProducto () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of idPedido
   * @param newVar the new value of idPedido
   */
  private void setIdPedido (int newVar) {
    idPedido = newVar;
  }

  /**
   * Get the value of idPedido
   * @return the value of idPedido
   */
  private int getIdPedido () {
    return idPedido;
  }

  /**
   * Set the value of idProducto
   * @param newVar the new value of idProducto
   */
  private void setIdProducto (int newVar) {
    idProducto = newVar;
  }

  /**
   * Get the value of idProducto
   * @return the value of idProducto
   */
  private int getIdProducto () {
    return idProducto;
  }

}
