package utils;

public class ObjetoDAO {

  static private void config;
  static private void sessionFactory;
  private void session;
  private void tx;  public ObjetoDAO () { };
  
  //
  // Methods
  //


  /**
   * Get the value of config
   * @return the value of config
   */
  private void getConfig () {
    return config;
  }

  /**
   * Get the value of sessionFactory
   * @return the value of sessionFactory
   */
  private void getSessionFactory () {
    return sessionFactory;
  }

  /**
   * Set the value of session
   * @param newVar the new value of session
   */
  private void setSession (void newVar) {
    session = newVar;
  }

  /**
   * Get the value of session
   * @return the value of session
   */
  private void getSession () {
    return session;
  }

  /**
   * Set the value of tx
   * @param newVar the new value of tx
   */
  private void setTx (void newVar) {
    tx = newVar;
  }

  /**
   * Get the value of tx
   * @return the value of tx
   */
  private void getTx () {
    return tx;
  }

  //
  // Other methods
  //

  /**
   * @param        registro
   */
  public void add(undef registro)
  {
  }


  /**
   * @param        registro
   */
  public void update(undef registro)
  {
  }


  /**
   * @param        registro
   */
  public void remove(undef registro)
  {
  }


  /**
   * @param        sqlQuery
   */
  public void selectQuery(undef sqlQuery)
  {
  }


  /**
   * @param        sqlQuery
   * @param        maxResult
   */
  public void selectQuery(undef sqlQuery, undef maxResult)
  {
  }


  /**
   * @param        sqlQuery
   */
  public void selectSQLQuery(undef sqlQuery)
  {
  }


  /**
   * @param        sqlQuery
   */
  public void executeSQLQuery(undef sqlQuery)
  {
  }


  /**
   */
  protected void getSession()
  {
  }


  /**
   */
  protected void getTransaction()
  {
  }


  /**
   */
  protected void startOperationBD()
  {
  }


}
