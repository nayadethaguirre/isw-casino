package utils;

abstract public class ManagerSession {

  static private void map;  public ManagerSession () { };
  
  //
  // Methods
  //


  /**
   * Get the value of map
   * @return the value of map
   */
  private void getMap () {
    return map;
  }

  //
  // Other methods
  //

  /**
   * @param        key
   */
  public static void removeObject(undef key)
  {
  }


  /**
   * @param        key
   */
  public static void findObject(undef key)
  {
  }


  /**
   * @param        key
   * @param        object
   */
  public static void saveObject(undef key, undef object)
  {
  }


}
