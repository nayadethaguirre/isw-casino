package utils;

abstract public class ObjetoService {

  private void objetoDAO;  public ObjetoService () { };
  
  //
  // Methods
  //


  /**
   * Set the value of objetoDAO
   * @param newVar the new value of objetoDAO
   */
  private void setObjetoDAO (void newVar) {
    objetoDAO = newVar;
  }

  /**
   * Get the value of objetoDAO
   * @return the value of objetoDAO
   */
  private void getObjetoDAO () {
    return objetoDAO;
  }

  //
  // Other methods
  //

  /**
   * @param        object
   */
  public void add(undef object)
  {
  }


  /**
   * @param        object
   */
  public void update(undef object)
  {
  }


  /**
   * @param        object
   */
  public void remove(undef object)
  {
  }


}
