package utils;

abstract public class StandardClassPK {

  static private void serialVersionUID;  public StandardClassPK () { };
  
  //
  // Methods
  //


  /**
   * Get the value of serialVersionUID
   * @return the value of serialVersionUID
   */
  private void getSerialVersionUID () {
    return serialVersionUID;
  }

  //
  // Other methods
  //

  /**
   * @param        obj
   */
  public void equals(undef obj)
  {
  }


  /**
   */
  public void hashCode()
  {
  }


}
